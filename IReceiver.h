//
// Created by Arthur on 11.09.2018.
//

#ifndef ISS_TEST_IRECEIVER_H
#define ISS_TEST_IRECEIVER_H

struct IReceiver
{
    virtual void Receive(const char* data, unsigned int size) = 0;
};

#endif //ISS_TEST_IRECEIVER_H
