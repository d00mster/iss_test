#include <cstring>
#include <array>
#include <chrono>
#include <random>
#include <thread>

#include "MyReceiver.h"
#include "MyCallback.h"

using std::chrono::high_resolution_clock;
using namespace std::chrono_literals;

const int rate = 500;
const char *sign{"\r\n\r\n"};
const size_t sign_size = 4;
const size_t min_packet_size = 0;
const size_t max_packet_size = 4092;
const size_t chunk_size = 1024;

int sendPacket(MyReceiver& receiver, const char* data, size_t packet_size) {
    int offset{0};
    int chunks = std::ceil(packet_size / static_cast<double>(chunk_size));
    for (int i = 0; i < chunks; ++i) {
        unsigned int size = chunk_size < packet_size ? chunk_size : packet_size;
        receiver.Receive(data+offset, size);
        packet_size-=chunk_size;
        offset += size;
        std::this_thread::sleep_for(2ms);
    }
}

void sendTextPacket(MyReceiver& receiver, unsigned int packet_size) {
    std::vector<char> data(packet_size + sign_size, 'a');
    std::memcpy(&data[packet_size], sign, sign_size);
    sendPacket(receiver, data.data(), data.size());
}

void sendBinaryPacket(MyReceiver& receiver, unsigned int packet_size) {
    std::vector<char> data(1 + 4 + packet_size, 'a');
    data[0] = 0x24;
    std::memcpy(&data[1], &packet_size, sizeof(uint32_t));
    sendPacket(receiver, data.data(), data.size());
}

int main() {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> idis(min_packet_size, max_packet_size);
    std::bernoulli_distribution bdis(0.5);

    MyCallback callback;
    MyReceiver receiver(&callback, max_packet_size);

    for (int i = 0; i < 100; ++i) {
        auto bytes_to_sent = idis(gen);
        if (bdis(gen)) {
            sendTextPacket(receiver, bytes_to_sent);
        }
        else {
            sendBinaryPacket(receiver, bytes_to_sent);
        }
    }

    return 0;
}