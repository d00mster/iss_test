//
// Created by Arthur on 11.09.2018.
//

#ifndef ISS_TEST_MYCALLBACK_H
#define ISS_TEST_MYCALLBACK_H


#include "ICallback.h"

class MyCallback : public ICallback {
    void BinaryPacket(const char* data, unsigned int size) override;
    void TextPacket(const char* data, unsigned int size) override;
};


#endif //ISS_TEST_MYCALLBACK_H
