//
// Created by Arthur on 12.09.2018.
//

#ifndef ISS_TEST_BINAYREADER_H
#define ISS_TEST_BINAYREADER_H

#include <vector>

#include "IReader.h"

struct ICallback;

class BinayReader : public IReader {
    ICallback *callback_{nullptr};
    size_t binary_packet_size_{0};
    bool is_data_ready_{false};
    bool is_new_packet_{true};
    std::vector<char> buffer_;
public:
    BinayReader(ICallback *callback, size_t buff_size);

    void Read(const char* data, unsigned int size) override ;

    bool IsDataReady() const override;

    void reset() override;
};


#endif //ISS_TEST_BINAYREADER_H
