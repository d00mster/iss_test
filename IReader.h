//
// Created by Arthur on 12.09.2018.
//

#ifndef ISS_TEST_IREADER_H
#define ISS_TEST_IREADER_H

#include <cstdint>

class IReader {
public:
    virtual ~IReader() = default;

    virtual void Read(const char* data, unsigned int size) = 0;

    virtual bool IsDataReady() const = 0;

    virtual void reset() = 0;
};

#endif //ISS_TEST_IREADER_H
