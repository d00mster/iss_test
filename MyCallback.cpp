//
// Created by Arthur on 11.09.2018.
//

#include <iostream>

#include "MyCallback.h"

void MyCallback::BinaryPacket(const char *data, unsigned int size) {
//    std::cout << "recived binary " << size << std::endl;
}

void MyCallback::TextPacket(const char *data, unsigned int size) {
//    std::cout << "recived text " << size << std::endl;
}
