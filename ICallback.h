//
// Created by Arthur on 11.09.2018.
//

#ifndef ISS_TEST_ICALLBACK_H
#define ISS_TEST_ICALLBACK_H

struct ICallback
{
    virtual void BinaryPacket(const char* data, unsigned int size) = 0;
    virtual void TextPacket(const char* data, unsigned int size) = 0;
};

#endif //ISS_TEST_ICALLBACK_H
