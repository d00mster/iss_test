//
// Created by Arthur on 12.09.2018.
//

#include "BinayReader.h"

#include <cstdint>
#include <cstring>
#include <algorithm>
#include <iostream>

#include "ICallback.h"

BinayReader::BinayReader(ICallback *callback, size_t buff_size) :
    callback_(callback)
{
    buffer_.reserve(buff_size);
}

void BinayReader::Read(const char *data, unsigned int size) {
    if (data == nullptr || size == 0)
        return;

    auto begin = data;
    if (is_new_packet_) {
        begin += sizeof(uint32_t);
        size -= sizeof(uint32_t);
        std::memcpy(&binary_packet_size_,  data, sizeof(uint32_t));
        is_new_packet_ = false;
    }
    auto end = begin + size;
    std::copy(begin, end, std::back_inserter(buffer_));

    if (buffer_.size() == binary_packet_size_) {
        callback_->BinaryPacket(buffer_.data(), static_cast<unsigned int>(buffer_.size()));
        is_data_ready_ = true;
    }
}

bool BinayReader::IsDataReady() const {
    return is_data_ready_;
}

void BinayReader::reset() {
    buffer_.resize(0);
    binary_packet_size_ = 0;
    is_data_ready_ = false;
    is_new_packet_ = true;
}
