//
// Created by Arthur on 12.09.2018.
//

#include "TextReader.h"

#include <cstring>
#include <algorithm>

#include "ICallback.h"

TextReader::TextReader(ICallback *callback, size_t buff_size) :
        callback_(callback)
{
    buffer_.reserve(buff_size);
}

void TextReader::Read(const char *data, unsigned int size) {
    auto end = data + size;
    if (std::strncmp(end - sign_size, sign, sign_size) == 0) {
        end -= sign_size;
        is_data_ready_ = true;
    }
    std::copy(data, end, std::back_inserter(buffer_));

    if (is_data_ready_) {
        callback_->TextPacket(buffer_.data(), static_cast<unsigned int>(buffer_.size()));
    }
}

bool TextReader::IsDataReady() const {
    return is_data_ready_;
}

void TextReader::reset() {
    buffer_.resize(0);
    is_data_ready_ = false;
}
