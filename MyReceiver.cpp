//
// Created by Arthur on 11.09.2018.
//

#include "MyReceiver.h"

#include "ICallback.h"

MyReceiver::MyReceiver(ICallback *callback, size_t buff_size) :
    text_reader_(callback, buff_size),
    binay_reader_(callback, buff_size)
{}

void MyReceiver::Receive(const char *data, unsigned int size) {
    if (data == nullptr || size == 0)
        return;

    auto first = data;
    if (is_new_packet_) {
        if (data[0] == 0x24) {
            first += 1;
            size -= 1;
            reader_ = &binay_reader_;
        }
        else {
            reader_ = &text_reader_;
        }
        is_new_packet_ = false;
    }
    reader_->Read(first, size);

    if (reader_->IsDataReady()) {
        reader_->reset();
        is_new_packet_ = true;
    }
}
