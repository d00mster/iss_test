//
// Created by Arthur on 11.09.2018.
//

#ifndef ISS_TEST_MYRECEIVER_H
#define ISS_TEST_MYRECEIVER_H

#include <vector>

#include "IReceiver.h"
#include "BinayReader.h"
#include "TextReader.h"

struct ICallback;

class MyReceiver : public IReceiver {
    BinayReader binay_reader_;
    TextReader text_reader_;
    bool is_new_packet_{true};
    IReader* reader_;
public:
    MyReceiver(ICallback *callback, size_t buff_size);

    void Receive(const char* data, unsigned int size) override;
private:
    void reset();
};


#endif //ISS_TEST_MYRECEIVER_H
