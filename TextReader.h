//
// Created by Arthur on 12.09.2018.
//

#ifndef ISS_TEST_TEXTREADER_H
#define ISS_TEST_TEXTREADER_H

#include <vector>

#include "IReader.h"

struct ICallback;

class TextReader : public IReader {
    ICallback *callback_{nullptr};
    const char *sign{"\r\n\r\n"};
    static constexpr size_t sign_size{4};
    bool is_data_ready_{false};
    std::vector<char> buffer_;
public:
    TextReader(ICallback *callback, size_t buff_size);

    void Read(const char* data, unsigned int size) override;

    bool IsDataReady() const override;

    void reset() override;
};


#endif //ISS_TEST_TEXTREADER_H
